import unittest, json
from models import AdRequest, convert_to_adrequest

class TestModels(unittest.TestCase):

    def test_convert_to_adrequest(self):
        jsn = json.loads('''{"site":{"id":"foo123","page":"http://www.foo.com/why-foo"},"device":{"ip":"69.250.196.118"},"user":{"id":"9cb89r"}}''')
        adrequest = convert_to_adrequest(jsn)
        self.assertEqual(adrequest.site.id, "foo123")
        self.assertEqual(adrequest.site.page, "http://www.foo.com/why-foo")
        self.assertEqual(adrequest.device.ip, "69.250.196.118")
        self.assertEqual(adrequest.user.id, "9cb89r")

        jsn = json.loads('''{"site":{"id":"foo123","page":"http://www.foo.com/why-foo"},"device":{"ip":"69.250.196.118"},"user":{}}''')
        adrequest = convert_to_adrequest(jsn)
        self.assertEqual(adrequest.site.id, "foo123")
        self.assertEqual(adrequest.site.page, "http://www.foo.com/why-foo")
        self.assertEqual(adrequest.device.ip, "69.250.196.118")
        self.assertEqual(adrequest.user.id, None)

        jsn = json.loads('''{"site":{"id":"foo123","page":"http://www.foo.com/why-foo"},"device":{"ip":"69.250.196.118"}}''')
        adrequest = convert_to_adrequest(jsn)
        self.assertEqual(adrequest.site.id, "foo123")
        self.assertEqual(adrequest.site.page, "http://www.foo.com/why-foo")
        self.assertEqual(adrequest.device.ip, "69.250.196.118")
        self.assertEqual(adrequest.user.id, None)


    def test_update_adrequest(self):
        jsn = json.loads('''{"site":{"id":"foo123","page":"http://www.foo.com/why-foo"},"device":{"ip":"69.250.196.118"},"user":{"id":"9cb89r"}}''')
        adrequest = convert_to_adrequest(jsn)
        enriched_data = {'pub_id': 'ksjdf9325', 'pub_name': 'ACME Inc.', 'demo_female': 49.3539043079894, 'location': 'US'}
        adrequest.update(enriched_data)

        self.assertEqual(adrequest.site.id, "foo123")
        self.assertEqual(adrequest.site.page, "http://www.foo.com/why-foo")
        self.assertEqual(adrequest.device.ip, "69.250.196.118")
        self.assertEqual(adrequest.user.id, "9cb89r")
        self.assertEqual(adrequest.site.publisher.id, "ksjdf9325")
        self.assertEqual(adrequest.site.publisher.name, "ACME Inc.")
        self.assertEqual(adrequest.site.demographics.female_percent, 49.3539043079894)
        self.assertEqual(adrequest.site.demographics.male_percent, 50.6460956920106)
        self.assertEqual(adrequest.device.geo, "US")


        jsn = json.loads('''{"site":{"id":"foo123","page":"http://www.foo.com/why-foo"},"device":{"ip":"69.250.196.118"},"user":{"id":"9cb89r"}}''')
        adrequest = convert_to_adrequest(jsn)
        enriched_data = {'pub_id': 'ksjdf9325', 'pub_name': None, 'demo_female': None, 'location': None}
        adrequest.update(enriched_data)

        self.assertEqual(adrequest.site.id, "foo123")
        self.assertEqual(adrequest.site.page, "http://www.foo.com/why-foo")
        self.assertEqual(adrequest.device.ip, "69.250.196.118")
        self.assertEqual(adrequest.user.id, "9cb89r")
        self.assertEqual(adrequest.site.publisher.id, "ksjdf9325")
        self.assertEqual(adrequest.site.publisher.name, None)
        self.assertEqual(adrequest.site.demographics.female_percent, None)
        self.assertEqual(adrequest.site.demographics.male_percent, None)
        self.assertEqual(adrequest.device.geo, None)

        jsn = json.loads('''{"site":{"id":"foo123","page":"http://www.foo.com/why-foo"},"device":{"ip":"69.250.196.118"}}''')
        adrequest = convert_to_adrequest(jsn)
        enriched_data = {'pub_id': 'ksjdf9325', 'pub_name': 'ACME Inc.', 'demo_female': 49.3539043079894, 'location': 'US'}
        adrequest.update(enriched_data)

        self.assertEqual(adrequest.site.id, "foo123")
        self.assertEqual(adrequest.site.page, "http://www.foo.com/why-foo")
        self.assertEqual(adrequest.device.ip, "69.250.196.118")
        self.assertEqual(adrequest.user.id, None)
        self.assertEqual(adrequest.site.publisher.id, "ksjdf9325")
        self.assertEqual(adrequest.site.publisher.name, "ACME Inc.")
        self.assertEqual(adrequest.site.demographics.female_percent, 49.3539043079894)
        self.assertEqual(adrequest.site.demographics.male_percent, 50.6460956920106)
        self.assertEqual(adrequest.device.geo, "US")

if __name__ == '__main__':
    unittest.main()