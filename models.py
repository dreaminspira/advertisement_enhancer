class AdRequest():

    def __init__(self, site, device, user=None):
        self.site = site
        self.device = device
        self.user = user
    
    def __repr__(self):
        return "AdRequest({}, {}, {})".format(self.site, self.device, self.user.id if self.user else None)

    def update(self, enriched_info):
        self.site.publisher = Publisher(enriched_info["pub_id"], enriched_info["pub_name"])
        demo_male = 100 - enriched_info["demo_female"] if enriched_info["demo_female"] else None
        self.site.demographics = Demographics(enriched_info["demo_female"], demo_male)
        self.device.geo = enriched_info["location"]

class Site():

    def __init__(self, id, page, demographics=None, publisher=None):
        self.id = id
        self.page = page
        self.demographics = demographics
        self.publisher = publisher
    
    def __repr__(self):
        return "Site({}, {}, demographics={}, publisher={})".format(self.id, self.page, self.demographics, self.publisher)


class Device():

    def __init__(self, ip, geo=None):
        self.ip = ip
        self.geo = geo

    def __repr__(self):
        return "Device({}, {})".format(self.ip, self.geo)

class User():

    def __init__(self, id=None):
        self.id = id
    
    def __repr__(self):
        return "User({})".format(self.id)

class Demographics():

    def __init__(self, female_percent, male_percent):
        self.female_percent = female_percent
        self.male_percent = male_percent
    
    def __repr__(self):
        return "Demographic({}, {})".format(self.female_percent, self.male_percent)

class Publisher():

    def __init__(self, id, name):
        self.id = id
        self.name = name
    
    def __repr__(self):
        return "Publisher({}, {})".format(self.id, self.name)

class Geo():

    def __init__(self, country):
        self.country = country
    
    def __repr__(self):
        return "Geo({})".format(self.country)

def convert_to_adrequest(jsn):
    site = Site(jsn["site"]["id"], jsn["site"]["page"])
    device = Device(jsn["device"]["ip"])
    try:
        user = User(jsn["user"]["id"])
    except KeyError:
        user = User()
    return AdRequest(site, device, user)