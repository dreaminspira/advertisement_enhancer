Steps to run

1. Create a virtualenv env
2. pip install -r requirements.txt
3. export FLASK_APP=main.py
4. flask run

To run performance test: locust --host=http://127.0.0.1:5000/