from locust import HttpLocust, TaskSet, task
from random_word import RandomWords


class UserBehavior(TaskSet):

    @task(1)
    def with_user(self):
        r = RandomWords()
        self.client.headers['Content-Type'] = "application/json"
        self.client.post("enhance", json={"site":{"id": r.get_random_word(),"page":"http://www." + r.get_random_word() + ".com/"},"device":{"ip":"69.250.196.118"},"user":{"id":"9cb89r"}})
    

    @task(1)
    def without_user(self):
        r = RandomWords()
        self.client.headers['Content-Type'] = "application/json"
        self.client.post("enhance", json={"site":{"id": r.get_random_word(),"page":"http://www." + r.get_random_word() + ".com/"},"device":{"ip":"69.250.196.118"}})


class WebsiteUser(HttpLocust):
    task_set = UserBehavior
    min_wait = 1000
    max_wait = 3000