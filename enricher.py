import requests
from exceptions import PublisherIdNotFoundError

class Enricher():

    def __init__(self, site_id, ip):
        self.site_id = site_id
        self.ip = ip

    def get_publisher_info(self):
        body = '{{"q":{{"siteID":"{}"}}}}'.format(self.site_id)
        headers = {'Content-Type': 'application/json'}
        r = requests.post("http://159.89.185.155:3000/api/publishers/find",data=body, headers=headers).json()
        try:
            pub_id = r['publisher']['id']
        except KeyError:
            raise PublisherIdNotFoundError
        try:
            pub_name = r['publisher']['name']
        except KeyError:
            pub_name = None
        return pub_id, pub_name

    
    def get_demographics_info(self):
        r = requests.get("http://159.89.185.155:3000/api/sites/" + self.site_id + "/demographics").json()
        try:
            demo_female = r['demographics']['pct_female']
        except KeyError:
            demo_female = None
        return demo_female
    
    def get_location_info(self):
        r = requests.get("https://ipapi.co/" + self.ip + "/json").json()
        try:
            country = r['country']
        except KeyError:
            country = None
        return country
    
    def get_info(self):
        info = {}
        info["pub_id"], info["pub_name"] = self.get_publisher_info()
        info["demo_female"] = self.get_demographics_info()
        info["location"] = self.get_location_info()
        return info
