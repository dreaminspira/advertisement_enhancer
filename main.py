from flask import Flask, request, json, abort
from models import AdRequest, convert_to_adrequest
from enricher import Enricher
from exceptions import PublisherIdNotFoundError

app = Flask(__name__)

@app.route('/enhance', methods=['POST'])
def enhance():
    try:
        adrequest = convert_to_adrequest(request.get_json())
        enricher = Enricher(adrequest.site.id, adrequest.device.ip)
        enriched_info = enricher.get_info()
        adrequest.update(enriched_info)
        return json.dumps(adrequest, default=lambda o: o.__dict__, indent=4)
    except PublisherIdNotFoundError:
        abort(500)
